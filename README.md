# 01-static-nodejs-grupo01

Para la instalación de dependencias ejecutar: 
```bash
npm install
```

Instalacion de sqlite
```
npm install --save sqlite3

```
crear el archivo de conexion.js en la carpeta db con el siguiente codigo
```
const sqlite3 = require("sqlite3").verbose();

const db = new sqlite3.Database(
  "./db/integrantes.sqlite",
  sqlite3.OPEN_READWRITE,
  (error) => {
    if (error) console.log("Ocurrio un error", error.message);
    else {
      console.log("Conexion Exitosa");
      db.run("select * from integrante");
    }
  }
);

async function getALL(query) {
  return new Promise((resolve, reject) => {
    db.all(query, (error, rows) => {
      if (error) return reject(err);
      resolve(rows);
    });
  });
}

module.exports = { db, getALL };
```


crear un archivo crebas.sql para la estructura del proyecto
```
CREATE TABLE IF NOT EXISTS Integrante (
  matricula VARCHAR(255) PRIMARY KEY, 
  nombre VARCHAR(255) NOT NULL,
  apellido VARCHAR(255) NOT NULL,
  rol VARCHAR(255) NOT NULL,
  codigo VARCHAR(255) NOT NULL,
  url VARCHAR(255) NOT NULL,
  esta_borrado BOOLEAN DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS Media (
  id INTEGER PRIMARY KEY, -- Agregamos una nueva columna para ser la clave primaria
  matricula VARCHAR(255) NOT NULL, -- Quitamos la restricción de clave primaria de esta columna
  url VARCHAR(255) NOT NULL,
  titulo VARCHAR(255) NOT NULL,
  parrafo TEXT NOT NULL,
  imagen TEXT,
  video TEXT,
  html TEXT,
  tipo_medio VARCHAR(255) NOT NULL,
  esta_borrado BOOLEAN DEFAULT FALSE,
  FOREIGN KEY (matricula) REFERENCES Integrante(matricula), -- Agregamos una restricción de clave externa
  FOREIGN KEY (tipo_medio) REFERENCES TipoMedio(tipo)
);

CREATE TABLE IF NOT EXISTS TipoMedio (
  tipo VARCHAR(255) PRIMARY KEY,  
  descripcion TEXT NOT NULL,
  esta_borrado BOOLEAN DEFAULT FALSE
);

CREATE INDEX IF NOT EXISTS idx_media_tipo ON Media(tipo_medio);
CREATE INDEX IF NOT EXISTS idx_media_integrante ON Media(matricula);

```
luego crear el archivo inserts.sql para agregar datos en las tabals Integrante,TipoMedio,Media

para ejecutar el proyecto ejecutar el siguiente codigo
```
npm run dev
```

Este archivo README proporciona instrucciones detalladas para instalar dependencias,configuracion de la base de datos SQLite3 y definir las tablas necesarias
